package com.example.alex.jsonservice;

import android.content.Context;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by alex on 09.08.2017.
 */

//Change adapter from ArrayList<Person> to ArrayList<Parcelable>
public class PersonsAdapter extends RecyclerView.Adapter<PersonsAdapter.PersonViewHolder> {

    ArrayList<Parcelable> persons;

    public PersonsAdapter(ArrayList<Parcelable> persons) {
        // super(context);
        this.persons = persons;
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
        return new PersonViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PersonViewHolder holder, int position) {

        Person person = (Person) persons.get(position);

        holder.tvName.setText("Name: " + person.getName().toString());
        holder.tvWeight.setText("Weight: " + person.getWeight().toString());
        holder.imageView.setImageResource(R.drawable.ic_android_black_24dp);
    }


    @Override
    public int getItemCount() {
        return persons.size();
    }

    public class PersonViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName;
        public TextView tvWeight;
        public ImageView imageView;


        public PersonViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvWeight = (TextView) itemView.findViewById(R.id.tvWeight);
            imageView = (ImageView) itemView.findViewById(R.id.ivImage);
        }
    }
}
