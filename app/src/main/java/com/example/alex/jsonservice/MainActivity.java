package com.example.alex.jsonservice;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements PersonsResultReceiver.Receiver{

    public static final  String LOG_TAG = "myLog";

    RecyclerView rwMain;
    EditText etWeight;
    Button btnGet;
    PersonsResultReceiver personsResultReceiver;
    ProgressBar pbMain;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rwMain = (RecyclerView) findViewById(R.id.rwMain);
        pbMain = (ProgressBar) findViewById(R.id.pbMain);

        //Creating ResultReceiver and set MainActivity as a Receiver
        personsResultReceiver = new PersonsResultReceiver(new Handler());
        personsResultReceiver.setReceiver(this);

        //
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rwMain.setLayoutManager(layoutManager);

        etWeight =(EditText) findViewById(R.id.etWeight);
        btnGet = (Button) findViewById(R.id.btnGet);
        btnGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String weight = etWeight.getText().toString();
                Intent intent = new Intent( MainActivity.this, PersonsService.class);
                intent.putExtra("weight", weight);
                intent.putExtra("receiver", personsResultReceiver);
                pbMain.setVisibility(View.VISIBLE);
                startService(intent);
            }
        });
    }

    @Override
    public void OnResultReceive(int ResultCode, Bundle bundle) {


        ArrayList<Parcelable> parcelables = new ArrayList<>();
        try
        {
            parcelables = bundle.getParcelableArrayList("persons");
        }
        catch (Exception e){
            e.printStackTrace();
        }
        initListArrayList(parcelables);
    }


    private void initListArrayList(ArrayList<Parcelable> persons){
       // ArrayList<Person> personArrayList = persons;
        PersonsAdapter personsAdapter = new PersonsAdapter(persons);
        rwMain.setAdapter(personsAdapter);
        pbMain.setVisibility(View.GONE);
    }
}
