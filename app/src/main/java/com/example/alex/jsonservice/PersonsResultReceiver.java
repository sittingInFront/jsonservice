package com.example.alex.jsonservice;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.os.ResultReceiver;

/**
 * Created by alex on 09.08.2017.
 */

public class PersonsResultReceiver extends ResultReceiver {

    public void setReceiver(Receiver mReceiver) {
        this.mReceiver = mReceiver;
    }

    private Receiver mReceiver;

    /**
     * Create a new ResultReceive to receive results.  Your
     * {@link #onReceiveResult} method will be called from the thread running
     * <var>handler</var> if given, or from an arbitrary thread if null.
     *
     * @param handler
     */
    public PersonsResultReceiver(Handler handler) {
        super(handler);
    }

    public interface  Receiver {
        void OnResultReceive(int ResultCode, Bundle bundle);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        super.onReceiveResult(resultCode, resultData);
        mReceiver.OnResultReceive(resultCode, resultData);
    }

}
