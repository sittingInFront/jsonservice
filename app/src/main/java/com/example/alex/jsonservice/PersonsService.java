package com.example.alex.jsonservice;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.os.ResultReceiver;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by alex on 09.08.2017.
 */

public class PersonsService extends IntentService {

    public PersonsService() {
        super("2434");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        String weight = intent.getStringExtra("weight");
        ResultReceiver personsResultReceiver = intent.getParcelableExtra("receiver");
        ArrayList<Person> persons = new ArrayList<>();
        String result = null;
        try {
            URL url = new URL("http://old.dietagram.ru?weight=" + weight);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setRequestProperty("Content-Type", "application/json");
            httpURLConnection.setRequestMethod("GET");
            BufferedReader bufferedReader =
                    new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line ;
            if (httpURLConnection.getResponseCode() == 200) {
                while ((line=bufferedReader.readLine())!=null){
                    sb.append(line);
                }
                result = sb.toString();
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = new JSONArray(jsonObject.getString("users"));
                for (int i = 0; i < jsonArray.length(); i++){
                    JSONObject item = jsonArray.getJSONObject(i);
                    Person person = new Person();
                    person.setName(item.getString("name"));
                    person.setWeight(item.getString("weight"));
                    person.setSex(Integer.valueOf(item.getString("sex")));
                    persons.add(person);
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("persons", persons);
        personsResultReceiver.send(1, bundle);
    }
}
