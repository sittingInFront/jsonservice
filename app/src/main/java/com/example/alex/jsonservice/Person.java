package com.example.alex.jsonservice;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by alex on 09.08.2017.
 */

//We need implement Parcelable interface to sending data via Bundle

public class Person implements Parcelable {
    private String name;
    private String weight;
    private int sex;

    public Person() {
    }

    public Person(String name, String weight) {
        this.name = name;
        this.weight = weight;

    }

    public Person(Parcel in) {
        name = in.readString();
        weight = in.readString();
        sex = in.readInt();
    }

    public static final Creator<Person> CREATOR = new Creator<Person>() {
        @Override
        public Person createFromParcel(Parcel in) {
            return new Person(in);
        }

        @Override
        public Person[] newArray(int size) {
            return new Person[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(weight);
        dest.writeInt(sex);
    }
}
